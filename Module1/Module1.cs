using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            SwapItems();
            GetMinimumValue();
        }

        int c;
        public int[] SwapItems(int a, int b)
        {
            c = a;
            a = b;
            b = c;
        }

        int minValue;

        public int GetMinimumValue(int[] input)
        {
            minValue = input[0];
            for(int i = 1; i< input.Length; i++)
            {
                if (input[i] < minValue)
                {
                    minValue = input[i];
                }
            }
            Console.WriteLine(minValue);
        }
    }
}
